# Duo-DTMF-Hack

Bypass 2FA by recording [DTMF](https://en.wikipedia.org/wiki/Dual-tone_multi-frequency_signaling) tones in voicemail.

## Recorded Demonstration

[Watch the Demo Video](duo-dtmf-hack.mp4)

## How to repeat

1. Record a standard voice mail message and insert DTMF tones into the message.
2. Any online or local [DTMF tone generator](https://onlinetonegenerator.com/dtmf.html) should work.
3. Save the voice mail message as your default message when the call goes unanswered.
4. Finally, start a new web browser session and select phone call back as the 2FA option.

## How to mitigate

1. Forbid telephony 2FA methods (callback, SMS, etc.) in your 2FA environment.
2. Prefer YubiKey and TOTP 2FA methods (in that order).

## Notes

* Any 2FA vendor that allows telephony 2FA delivery methods is vulnerable to this attack. 
* Hackers could insert DTMF tones into voicemails or forward calls to phone systems that have pre-recorded voicemail messages with DTMF tones.
* Newer, virtual phone systems and soft phone/web clients are [more vulnerable](https://www.fcc.gov/consumers/guides/voicemail-hacking) to this attack.
